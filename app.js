var express = require('express');
var bodyParser = require('body-parser');
var multer = require('multer');
var storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, './uploads/')
    },
    filename: function(req, file, cb) {
        file.originalname = file.originalname.replace(/ /g, '');
        cb(null, file.fieldname + '-' + Date.now() + file.originalname)
    }
});
var upload = multer({
    storage: storage
});
// var form = require("express-form"),
//     field = form.field;
var fs = require('fs');
var path = require('path');
var app = express();
// выбираем модуль для отправки данных между хостами
var requestify = require('requestify');
// var http = require('http');

app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
// parse application/json
app.use(bodyParser.json());
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({
    extended: true
}));
// parse multipart/form-data
app.use(express.static(path.join(__dirname, 'public')));


// app.route('/')
app.get('/', function(req, res) {
    res.render('index');
})

app.post('/', function(req, res) {
  res.render('thanks');
})

app.get('/thanks', function(req, res) {
    res.render('thanks');
});

app.post('/uploads', upload.single('image'), function(req, res) {

    console.log('file', req.file);

    var link = {
        image: !!req.file ? req.protocol + "://" + req.get('host') + "/" + req.file.path : ""
    };

    console.log('data', link);

    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify(link));
});



/*
 *  Показать картинку
 */
app.get('/uploads/:id', function(req, res) {

    console.log(" __dirname  is ", __dirname);
    var options = {
        root: __dirname + '/uploads/',
        dotfiles: 'deny',
        headers: {
            'x-timestamp': Date.now(),
            'x-sent': true
        }
    };
    var imgName = req.params.id;

    res.sendFile(imgName, options, function(err) {
        if (err) {
            console.log(err);
            res.status(err.status).end();
        } else {
            console.log('Sent:', imgName);
        }
    });
})

app.listen(process.env.PORT || 3000);
