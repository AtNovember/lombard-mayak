/*
  HEADROOM
*/

// $("#header").headroom();
// $("#nav").headroom();

// $(function() {
//     var header = document.querySelector("#header");
//
//     if(window.location.hash) {
//       header.classList.add("headroom--unpinned");
//     }
//
//     var headroom = new Headroom(header, {
//         tolerance: {
//           down : 10,
//           up : 20
//         },
//         offset : 205
//     });
//     headroom.init();
//
// }());


/*
  SMOTHSCROLL
*/

$(function() {
    $('a[href*="#"]:not([href="#"], [href="#myCarousel"], [href="#buyHere"], [href="#moneyCarousel"])').click(function() {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            // console.log("asdasd", target);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html, body').animate({
                    scrollTop: target.offset().top,
                    // scroll
                }, 1000);
                return false;
            }
        }
    });
});


var lastScrollTop = 1000;
$(window).scroll(function(event){
   var st = $(this).scrollTop();
   if (st > lastScrollTop){
       // downscroll code
       $('#nav').fadeOut(700);
   } else {
      // upscroll code
      $('#nav').fadeIn(100);
   }
   lastScrollTop = st;
});



/*
  FORM HANDLER
*/

function checkStatus(response) {
    if (response.status >= 200 && response.status < 300) {
        console.log("200 - 300");
        return response
    } else {
        var error = new Error(response.statusText)
        console.log("err", error);
        error.response = response
        throw error
    }
}

function parseJSON(response) {
    return response.json()
}

var image;

function myFunction() {

    var input = document.querySelector('input[type="file"]')

    var data = new FormData()
    data.append('image', input.files[0])

    fetch('/uploads', {
            method: 'POST',
            body: data
        })
        .then(checkStatus)
        .then(parseJSON)
        .then(function(response) {
            console.log('request succeeded with JSON response', response)
            console.log('image', response.image);


            input = document.querySelector('input[type="file"]');
            var newInput = document.createElement('input');
            newInput.type = "text";
            newInput.name = "image";
            newInput.className = "form-control";
            newInput.value = response.image;
            // newInput.innerHTML = response.image;
            input.parentNode.replaceChild(newInput, input);

            // return response.text()
            return response.image;
        })
        .then(function(image) {
            console.log('got text', image)
        })
        .catch(function(error) {
            console.log('request failed', error)
        })


};
